//
//  CircleView.m
//  CMGtestapp
//
//  Created by salyasev on 23/12/2016.
//  Copyright © 2016 cmg. All rights reserved.
//

#import "CircleView.h"
#import "Helper.h"


@interface CircleView()
//@property (strong, nonatomic) CAShapeLayer *circleLayer;
@end

@implementation CircleView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [Helper randomColor];
        self.layer.cornerRadius = frame.size.height/2;
        
        /* alternative way to draw a circle via Layers
         
        self.circleLayer = [CAShapeLayer layer];
        CGPathRef path = [[UIBezierPath bezierPathWithOvalInRect:frame] CGPath];
        [self.circleLayer setPath:path];
        [self.circleLayer setFillColor:[[Helper randomColor] CGColor]];
         
        [[self layer] addSublayer:self.circleLayer];
        
        //Animation for Layers:
         CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
         animation.fromValue = [NSValue valueWithCGPoint:circleLayer.position];
         
         int y = self.animationView.bounds.size.height;
         animation.toValue = [NSValue valueWithCGPoint:CGPointMake(circleLayer.position.x, y)];
         //animation.duration = .5;
         animation.speed = [self randomFloatBetween:0 andLargerFloat:2];
         
         [circleLayer addAnimation:animation forKey:animation.keyPath];
        */
    }
    
    return self;
}

- (void)dealloc {
    // for way 2:
    // [self.circleLayer removeFromSuperlayer];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
