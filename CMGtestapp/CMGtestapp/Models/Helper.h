//
//  Helper.h
//  CMGtestapp
//
//  Created by salyasev on 23/12/2016.
//  Copyright © 2016 cmg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIColor+Hex.h"

static const int BACKGROUND_COLOR = 0xC0DE5E;
static const int CIRCLE_4_COLOR = 0x236EBF;
static const int CIRCLE_3_COLOR = 0x6D73BF;
static const int CIRCLE_2_COLOR = 0xFA78B2;
static const int CIRCLE_1_COLOR = 0xFCAF00;

@interface Helper : NSObject

+ (UIColor *)randomColor;
+ (float)randomFloatMin:(float)minValue
                    Max:(float)maxValue;

@end
