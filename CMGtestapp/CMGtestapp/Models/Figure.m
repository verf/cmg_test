//
//  Figure.m
//  CMGtestapp
//
//  Created by salyasev on 23/12/2016.
//  Copyright © 2016 cmg. All rights reserved.
//

#import "Figure.h"
#import "UIColor+Hex.h"
#import "Helper.h"

@implementation Figure

static const int PERCENT_OF_AREA_2 = 70;
static const int PERCENT_OF_AREA_3 = 50;
static const int PERCENT_OF_AREA_4 = 20;

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super init];
    if (self) {
        // setup 1st circle
        self.area1 = [[CircleView alloc] initWithFrame:frame];
        [self.area1 setBackgroundColor: [UIColor colorWithHex:CIRCLE_1_COLOR]];
        
        // setup 2d circle
        CGRect frameArea2 = [self calculateRectFromArea:frame
                                                percent:PERCENT_OF_AREA_2];
        self.area2 = [[CircleView alloc] initWithFrame:frameArea2];
        [self.area2 setBackgroundColor: [UIColor colorWithHex:CIRCLE_2_COLOR]];
        [self.area1 addSubview:self.area2];
        
        // setup 3d circle
        CGRect frameArea3 = [self calculateRectFromArea:frameArea2
                                                percent:PERCENT_OF_AREA_3];
        self.area3 = [[CircleView alloc] initWithFrame:frameArea3];
        [self.area3 setBackgroundColor: [UIColor colorWithHex:CIRCLE_3_COLOR]];
        [self.area2 addSubview:self.area3];
        
        // setup 4th circle
        CGRect frameArea4 = [self calculateRectFromArea:frameArea3
                                                percent:PERCENT_OF_AREA_4];
        self.area4 = [[CircleView alloc] initWithFrame:frameArea4];
        [self.area4 setBackgroundColor: [UIColor colorWithHex:CIRCLE_4_COLOR]];
        [self.area3 addSubview:self.area4];
    }
    return self;
}

- (float)percentArea:(int)percent
            fromRect:(CGRect)rect {
    if (percent > 100 || percent < 0) {
        return -1;
    }
    
    float originalArea = rect.size.height * rect.size.width;
    float requestedArea = originalArea * percent/100;
    return requestedArea;
}

- (CGRect)calculateRectFromArea:(CGRect)originalRect
                        percent:(int)percent {
    CGRect newRect = CGRectZero;
    float newArea = [self percentArea:percent
                             fromRect:originalRect];
    if (newArea > 0) {
        float sideOfNewSquare = sqrtf(newArea);
        float x = originalRect.size.width/2 - sideOfNewSquare/2;
        float y = originalRect.size.height/2 - sideOfNewSquare/2;
        newRect = CGRectMake(x, y, sideOfNewSquare, sideOfNewSquare);
    }
    return newRect;
}

@end
