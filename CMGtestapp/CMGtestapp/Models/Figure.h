//
//  Figure.h
//  CMGtestapp
//
//  Created by salyasev on 23/12/2016.
//  Copyright © 2016 cmg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CircleView.h"

@interface Figure : NSObject

@property (strong, nonatomic) CircleView *area1;
@property (strong, nonatomic) CircleView *area2;
@property (strong, nonatomic) CircleView *area3;
@property (strong, nonatomic) CircleView *area4;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithFrame:(CGRect)frame NS_DESIGNATED_INITIALIZER;

@end
