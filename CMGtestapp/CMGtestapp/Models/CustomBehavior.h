//
//  CustomBehavior.h
//  CMGtestapp
//
//  Created by salyasev on 23/12/2016.
//  Copyright © 2016 cmg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomBehavior : UIDynamicBehavior

- (void)addItem:(id <UIDynamicItem>)item;
- (void)removeItem:(id <UIDynamicItem>)item;

@end
