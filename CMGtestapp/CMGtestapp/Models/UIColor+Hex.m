//
//  UIColor+Hex.m
//  CMGtestapp
//
//  Created by salyasev on 23/12/2016.
//  Copyright © 2016 cmg. All rights reserved.
//

#import "UIColor+Hex.h"

@implementation UIColor (Hex)

+ (UIColor *)colorWithHex:(int)rgbHexValue {
    
    return [UIColor colorWithRed:((float)((rgbHexValue & 0xFF0000) >> 16))/255.0
                           green:((float)((rgbHexValue & 0x00FF00) >> 8))/255.0
                            blue:((float)(rgbHexValue & 0x0000FF))/255.0
                           alpha:1.0];
}

@end
