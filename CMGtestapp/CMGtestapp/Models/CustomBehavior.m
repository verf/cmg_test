//
//  CustomBehavior.m
//  CMGtestapp
//
//  Created by salyasev on 23/12/2016.
//  Copyright © 2016 cmg. All rights reserved.
//

#import "CustomBehavior.h"
#import "Helper.h"
#import "CircleView.h"

static const float GRAVITY_MAX_VALUE = 0.4;
static const float GRAVITY_MIN_VALUE = 0.01;

@interface CustomBehavior() <UICollisionBehaviorDelegate>

@property (strong, nonatomic) UIGravityBehavior *gravity;
@property (strong, nonatomic) UICollisionBehavior *collider;

@end

@implementation CustomBehavior

- (UIGravityBehavior *)gravity {
    if (!_gravity) {
        _gravity = [[UIGravityBehavior alloc] init];
        _gravity.gravityDirection = CGVectorMake(0.0, -1.0); // to up side
        _gravity.magnitude = [Helper randomFloatMin:GRAVITY_MIN_VALUE
                                                Max:GRAVITY_MAX_VALUE];
    }
    _gravity.magnitude = [Helper randomFloatMin:GRAVITY_MIN_VALUE
                                            Max:GRAVITY_MAX_VALUE];
    return _gravity;
}

- (UICollisionBehavior *)collider {
    if (!_collider) {
        _collider = [[UICollisionBehavior alloc] init];
        _collider.collisionMode = UICollisionBehaviorModeBoundaries;
        _collider.collisionDelegate = self;
    }
    return _collider;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self addChildBehavior:self.gravity];
        [self addChildBehavior:self.collider];
    }
    return self;
}

- (void)addItem:(id <UIDynamicItem>)item {
    [self.gravity addItem:item];
    [self.collider addItem:item];
}

- (void)removeItem:(id <UIDynamicItem>)item {
    [self.gravity removeItem:item];
    [self.collider removeItem:item];
}

#pragma mark - UICollisionBehaviorDelegate

- (void)collisionBehavior:(UICollisionBehavior *)behavior beganContactForItem:(id <UIDynamicItem>)item1 withItem:(id <UIDynamicItem>)item2 atPoint:(CGPoint)p {
    
    if (![item1 isKindOfClass:[CircleView class]] ||
        [item2 isKindOfClass:[CircleView class]]) {
        return;
    }
//    CircleView *circle1 = (CircleView *)item1;
//    CircleView *circle2 = (CircleView *)item2;
    
    [UIView animateWithDuration:1.0 animations:^{
        // todo: add logic for merge objects
    
    } completion:^(BOOL finished) {
        // todo
    }];
}
- (void)collisionBehavior:(UICollisionBehavior *)behavior endedContactForItem:(id <UIDynamicItem>)item1 withItem:(id <UIDynamicItem>)item2 {
    // todo
}

// The identifier of a boundary created with translatesReferenceBoundsIntoBoundary or setTranslatesReferenceBoundsIntoBoundaryWithInsets is nil
- (void)collisionBehavior:(UICollisionBehavior*)behavior beganContactForItem:(id <UIDynamicItem>)item withBoundaryIdentifier:(nullable id <NSCopying>)identifier atPoint:(CGPoint)p {
    // todo
}
- (void)collisionBehavior:(UICollisionBehavior*)behavior endedContactForItem:(id <UIDynamicItem>)item withBoundaryIdentifier:(nullable id <NSCopying>)identifier {
    // todo
}


@end
