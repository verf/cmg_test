//
//  Helper.m
//  CMGtestapp
//
//  Created by salyasev on 23/12/2016.
//  Copyright © 2016 cmg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@implementation Helper

+ (UIColor *)randomColor {
    switch (arc4random() % 5) {
        case 0:
            return [UIColor colorWithHex:CIRCLE_1_COLOR];
        case 1:
            return [UIColor colorWithHex:CIRCLE_2_COLOR];
        case 2:
            return [UIColor colorWithHex:CIRCLE_3_COLOR];
        case 3:
            return [UIColor colorWithHex:CIRCLE_4_COLOR];
        case 4:
            return [UIColor orangeColor];
    }
    return [UIColor blackColor];
}

static const long ARC4RANDOM_CUSTOM_MAX = 100000000;

+ (float)randomFloatMin:(float)minValue
                    Max:(float)maxValue {
    if (minValue > maxValue) {
        float temp = minValue;
        minValue = maxValue;
        maxValue = temp;
    }
    
    float range = maxValue - minValue;
    float val =  (float)(arc4random() % ((unsigned)ARC4RANDOM_CUSTOM_MAX)) / ARC4RANDOM_CUSTOM_MAX * range + minValue;
    return val;
}

@end
