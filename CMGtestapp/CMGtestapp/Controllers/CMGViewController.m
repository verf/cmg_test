//
//  ViewController.m
//  CMGtestapp
//
//  Created by salyasev on 22/12/2016.
//  Copyright © 2016 cmg. All rights reserved.
//

#import "CMGViewController.h"
#import "Figure.h"
#import "CustomBehavior.h"
#import "Helper.h"


@interface CMGViewController ()
@property (weak, nonatomic) IBOutlet UIView *animationView;
@property (strong, nonatomic) UIDynamicAnimator *animator;
@property (strong, nonatomic) CustomBehavior *customBehavior;
@property (strong, nonatomic) NSMutableArray <UIView *> * circleViews;
@property (nonatomic) BOOL enableAnimation;
@end

static const float RADIUS_MIN = 20;
static const float RADIUS_MAX = 200;

@implementation CMGViewController

- (UIDynamicAnimator *)animator {
    if (!_animator) {
        _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.animationView];
    }
    return _animator;
}

- (CustomBehavior *)customBehavior {
    if (!_customBehavior) {
        _customBehavior = [[CustomBehavior alloc] init];
        [self.animator addBehavior:_customBehavior];
    }
    return _customBehavior;
}

- (NSMutableArray <UIView *> *)circleViews {
    if (!_circleViews) {
        _circleViews = [[NSMutableArray alloc] init];
    }
    return _circleViews;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setup];
    
    // can make an additional shot using tap
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    [tap addTarget:self action:@selector(shot)];
    [self.animationView addGestureRecognizer:tap];
}

- (void)setup {
    self.animationView.backgroundColor = [UIColor colorWithHex:BACKGROUND_COLOR];
    self.enableAnimation = YES;
    
    [NSTimer scheduledTimerWithTimeInterval:0.1
                                     target:self
                                   selector:@selector(runAnimation)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)runAnimation {
    // generate figure at random time interval
    if(arc4random() % 5 == 1)
    {
        [self shot];
    }
}

- (void)shot {
    float radius = [Helper randomFloatMin:RADIUS_MIN
                                      Max:RADIUS_MAX];
    
    int x = [Helper randomFloatMin:-radius
                               Max:(self.animationView.bounds.size.width - radius)];
    
    int y = [Helper randomFloatMin:(self.animationView.bounds.size.height + radius*2)
                               Max:(self.animationView.bounds.size.height * 2)];
    
    CGRect frame = CGRectMake(x, y, radius*2, radius*2);
    
    Figure *figure = [[Figure alloc] initWithFrame:frame];
    [self.circleViews addObject:figure.area1];
    [self.animationView addSubview:figure.area1];
    
    // add behavior to object
    if (self.enableAnimation) {
        [self.customBehavior addItem:figure.area1];
        
        // handle case out of screen and removing the views
        __weak typeof(self) weakSelf = self;
        self.customBehavior.action = ^ {
            
            NSMutableArray *discardedItems = [NSMutableArray array];
            for (UIView *view in weakSelf.circleViews) {
                CGFloat currentY = view.center.y;
                
                if (currentY < -view.frame.size.height/2) {
                    [discardedItems addObject:view];
                    
                    [weakSelf.customBehavior removeItem:view];
                    [view removeFromSuperview];
                }
            }
            if ([discardedItems count]) {
                [weakSelf.circleViews removeObjectsInArray:discardedItems];
            }
        };
    }
}

@end
